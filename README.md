### These Userstyle will change the look of websites to make them easier on the eyes. ###

## Userstyle for the website [Meetup.com](https://www.meetup.com) ##
#### Userstyle Page: https://userstyles.org/styles/132933/meetup-com-dark ####
![Screenshot_110.png](https://bitbucket.org/repo/LGGgKE/images/14451812-Screenshot_110.png)

## Userstyle for the website [notepad.pw](http://notepad.pw) ##
#### Userstyle Page: https://userstyles.org/styles/132935/notepad-pw-dark ####
![Screenshot_94.png](https://bitbucket.org/repo/LGGgKE/images/1236868713-Screenshot_94.png)

## Userstyle for the website [itsalmo.st](http://itsalmo.st) ##
#### Userstyle Page: https://userstyles.org/styles/133119/itsalmo-st-dark ####
![Screenshot_100.png](https://bitbucket.org/repo/LGGgKE/images/1568966952-Screenshot_100.png)

## Userstyle for the website [Thingiverse.com](https://www.thingiverse.com) ##
#### Userstyle Page: https://userstyles.org/styles/133143/thingiverse-dark ####
![Screenshot_101.png](https://bitbucket.org/repo/LGGgKE/images/3891820581-Screenshot_101.png)

## Userstyle for the website [rocket.chat](https://demo.rocket.chat/) ##
#### Userstyle Page: https://userstyles.org/styles/133150/rocketchat-dark ####
![Screenshot_126.png](https://bitbucket.org/repo/LGGgKE/images/1204708834-Screenshot_126.png)

## Userstyle for the website [haskell.org](https://www.haskell.org/) ##
#### Userstyle Page: https://userstyles.org/styles/133189/haskell-org-dark ####
![Screenshot_105.png](https://bitbucket.org/repo/LGGgKE/images/3929341330-Screenshot_105.png)

## Userstyle for the website [downdetector.com](http://downdetector.com) ##
#### Userstyle Page: https://userstyles.org/styles/133259/downdetector-com-dark ####
![Screenshot_111.png](https://bitbucket.org/repo/LGGgKE/images/1725544948-Screenshot_111.png)

## Userstyle for the website [who.is](https://who.is/) ##
#### Userstyle Page: https://userstyles.org/styles/133308/who-is-dark ####
![Screenshot_123.png](https://bitbucket.org/repo/LGGgKE/images/1973902430-Screenshot_123.png)

## Userstyle for the website [debian.org](https://www.debian.org) ##
#### Userstyle Page: https://userstyles.org/styles/133401/debian-org-dark ####
![Screenshot_152.png](https://bitbucket.org/repo/LGGgKE/images/3554781783-Screenshot_152.png)

## Userstyle for the website [gitlab.com](https://gitlab.com) ##
#### Userstyle Page: https://userstyles.org/styles/133416/gitlab-dark ####
![Screenshot_154.png](https://bitbucket.org/repo/LGGgKE/images/2647397780-Screenshot_154.png)

## Userstyle for the website [digitalocean.com/community](https://www.digitalocean.com/community/) ##
#### Userstyle Page: https://userstyles.org/styles/133465/digitalocean-community-dark ####
![Screenshot_158.png](https://bitbucket.org/repo/LGGgKE/images/3438059901-Screenshot_158.png)

## Userstyle for the website [thehackernews.com](https://thehackernews.com/) ##
#### Userstyle Page: https://userstyles.org/styles/133490/thehackernews-com-dark ####
![Screenshot_162.png](https://bitbucket.org/repo/LGGgKE/images/3151613559-Screenshot_162.png)

## Userstyle for the website [pipl.com](https://pipl.com/) ##
#### Userstyle Page: https://userstyles.org/styles/133507/pipl-com-dark ####
![Screenshot_168.png](https://bitbucket.org/repo/LGGgKE/images/4174404509-Screenshot_168.png)

## Userstyle for the website [limetorrents.cc](https://www.limetorrents.cc/) ##
#### Userstyle Page: https://userstyles.org/styles/133509/limetorrents-cc-dark ####
![Screenshot_169.png](https://bitbucket.org/repo/LGGgKE/images/2659765235-Screenshot_169.png)

## Userstyle for the website [protonmail.com](https://mail.protonmail.com/) ##
#### Userstyle Page: https://userstyles.org/styles/133566/protonmail-com-dark ####
![Screenshot_171.png](https://bitbucket.org/repo/LGGgKE/images/3212156315-Screenshot_171.png)

## Userstyle for the website [torrentproject.se](http://torrentproject.se/) ##
#### Userstyle Page: https://userstyles.org/styles/133733/torrentproject-se-dark ####
![Screenshot_176.png](https://bitbucket.org/repo/LGGgKE/images/4246009009-Screenshot_176.png)

## Userstyle for the website [websdr.ewi.utwente.nl:8901](http://websdr.ewi.utwente.nl:8901/) ##
#### Userstyle Page: https://userstyles.org/styles/133734/websdr-ewi-utwente-nl-dark ####
![Screenshot_174.png](https://bitbucket.org/repo/LGGgKE/images/3408291889-Screenshot_174.png)

## Userstyle for the website [searx.me](https://searx.me/) ##
#### Userstyle Page: https://userstyles.org/styles/135487/searx-me-dark-v2 ####
![Screenshot_202.png](https://bitbucket.org/repo/LGGgKE/images/1462547834-Screenshot_202.png)

## Userstyle for the website [joindiaspora.com](https://joindiaspora.com) ##
#### Userstyle Page: https://userstyles.org/styles/135621/joindiaspora-com-dark ####
![Screenshot_211.png](https://bitbucket.org/repo/LGGgKE/images/1135681567-Screenshot_211.png)

## Userstyle for the website [minds.com](https://www.minds.com) ##
#### Userstyle Page: https://userstyles.org/styles/136052/minds-com-dark ####
![Screenshot_222.png](https://bitbucket.org/repo/LGGgKE/images/1851908404-Screenshot_222.png)

## Userstyle for the website [milo.yiannopoulos.net](https://milo.yiannopoulos.net/) ##
#### Userstyle Page: https://userstyles.org/styles/139163/milo-yiannopoulos-net-dark ####
![Screenshot_289.png](https://bitbucket.org/repo/LGGgKE/images/7804007-Screenshot_289.png)

## Userstyle for the website [fossbytes.com](https://fossbytes.com) ##
#### Userstyle Page: https://userstyles.org/styles/139764/fossbytes-com-dark ####
![Screenshot_305.png](https://bitbucket.org/repo/LGGgKE/images/3708572127-Screenshot_305.png)

## Userstyle for the website [privacytools.io](https://www.privacytools.io) ##
#### Userstyle Page: https://userstyles.org/styles/139765/privacytools-io-dark ####
![Screenshot_304.png](https://bitbucket.org/repo/LGGgKE/images/31713604-Screenshot_304.png)

## Userstyle for Dark Scroll Bar on every website ##
#### Userstyle Page: https://userstyles.org/styles/140455/dark-scroll-bar-on-everything ####
![DarkScrollCoverImage.png](https://bitbucket.org/repo/LGGgKE/images/4088598224-DarkScrollCoverImage.png)

## Userstyle for the website [reed.co.uk](https://www.reed.co.uk) ##
#### Userstyle Page: https://userstyles.org/styles/140717/reed-co-uk-dark ####
![reed.png](https://bitbucket.org/repo/LGGgKE/images/942087537-reed.png)